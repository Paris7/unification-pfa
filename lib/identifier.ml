type t = string [@@deriving eq, ord, show]

(** Generate new unique id *)
let fresh =
  let r = ref 0 in
  fun () ->
    let id = "id_" ^ string_of_int !r in
    incr r;
    id
;;
