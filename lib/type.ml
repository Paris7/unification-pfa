type t =
  | Var of Identifier.t
  | Int
  | Product of t * t
  | Arrow of t * t
[@@deriving eq, ord, show]

(** string of Type *)
let rec string_of_type = function
  | Var v -> "Var '" ^ v ^ "'"
  | Int -> "Int"
  | Product (a, b) -> "Product(" ^ string_of_type a ^ ", " ^ string_of_type b ^ ")"
  | Arrow (a, b) -> "Arrow(" ^ string_of_type a ^ ", " ^ string_of_type b ^ ")"
;;
