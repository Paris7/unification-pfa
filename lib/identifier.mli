type t = string [@@deriving eq, ord, show]

val fresh : unit -> t
