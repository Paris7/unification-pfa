type t = Type.t Map.Make(Identifier).t

val apply : t -> Type.t -> Type.t
val compose : t -> t -> t
val empty : t
val singleton : Identifier.t -> Type.t -> t
val find : Identifier.t -> t -> Type.t option
val to_string : t -> string
val equal : t -> t -> bool
