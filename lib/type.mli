type t =
  | Var of Identifier.t
  | Int
  | Product of t * t
  | Arrow of t * t
[@@deriving eq, ord, show]

(* Type to string *)
val string_of_type : t -> string
